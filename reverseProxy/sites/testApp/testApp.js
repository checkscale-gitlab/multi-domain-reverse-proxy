const express = require('express');

var app = express();

app.all("/", function(req, res) {
	res.send('Test site functional!');
});

module.exports.handler = app;